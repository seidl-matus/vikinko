package performace.test.testperformace;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class NetworkActivity extends AppCompatActivity {
    private static int faultdCounter = 1;
    private static int goodCounter = 1;

    private TextView txtCount, txtSuccesCount, txtFault;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network);

        txtCount = findViewById(R.id.id_count);
        txtSuccesCount = findViewById(R.id.id_count_succes);
        txtFault = findViewById(R.id.id_count_fault);

       // final String myUrl = "http://myApi.com/get_some_data";
        //final int numOfRepeat = 8;

        findViewById(R.id.id_start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HttpGetRequest getRequest = new HttpGetRequest();
                txtCount.setText("Count: 0");
                txtSuccesCount.setText("Succes: "+ 0);
                txtFault.setText("Fault: "+ 0);
                //getRequest.execute(myUrl,numOfRepeat);
                getRequest.execute(
                        String.valueOf(((EditText) findViewById(R.id.id_enter_url)).getText()),
                        String.valueOf(
                                                Integer.parseInt(
                                                        String.valueOf(
                                                                ((EditText) findViewById(R.id.id_num_of_call)).getText()
                                                        ).trim()
                                                )));
            }
        });


        System.out.println(goodCounter + " / " + faultdCounter);


    }

    class HttpGetRequest extends AsyncTask<String, Integer, Void> {

        public static final String REQUEST_METHOD = "GET";
        public static final int READ_TIMEOUT = 15000;
        public static final int CONNECTION_TIMEOUT = 15000;

        private int count = 0, faultCounter=0, succesCounter=0;

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            count++;
            txtCount.setText("Count: " +  count);
            if(values[0] >= 200 && values[0] <= 210){
                succesCounter++;
                txtSuccesCount.setText("Succes: "+ succesCounter);
            }else {
                faultCounter++;
                txtFault.setText("Fault: "+ faultCounter);
            }


        }

        @Override
        protected Void doInBackground(String... params) {
            String stringUrl = params[0];
            int numberOfRepeat = Integer.parseInt(params[1]);


                for (int i = 0; i < numberOfRepeat; i++) {
                    try {
                    //Create a URL object holding our url
                    URL myUrl = new URL(stringUrl);
                    //Create a connection
                    HttpURLConnection connection = (HttpURLConnection)
                            myUrl.openConnection();


                    //Set methods and timeouts
                    connection.setRequestMethod(REQUEST_METHOD);
                    connection.setReadTimeout(READ_TIMEOUT);
                    connection.setConnectTimeout(CONNECTION_TIMEOUT);

                    //Connect to our url
                    connection.connect();
                    connection.getResponseCode();

                    this.publishProgress( connection.getResponseCode());
                    } catch (IOException e) {
                        e.printStackTrace();
                        this.publishProgress( 500);

                    }
                }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            super.onPostExecute(result);
        }
    }
}
