package performace.test.testperformace;

public class Item {
    private int numOfLine;
    private int randValue;

    public Item(int numOfLine, int randValue) {
        this.numOfLine = numOfLine;
        this.randValue = randValue;
    }

    public Item() {

    }

    public int getNumOfLine() {
        return numOfLine;
    }

    public void setNumOfLine(int numOfLine) {
        this.numOfLine = numOfLine;
    }

    public int getRandValue() {
        return randValue;
    }

    public void setRandValue(int randValue) {
        this.randValue = randValue;
    }
}
