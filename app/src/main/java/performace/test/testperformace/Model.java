package performace.test.testperformace;

public class Model {

    private  int counter;

    private static Model instance;

    private Model(){}

    public static Model getInstance(){
        if(instance == null){
            instance = new Model();
        }
        return instance;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
