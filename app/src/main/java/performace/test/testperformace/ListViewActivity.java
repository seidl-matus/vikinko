package performace.test.testperformace;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ListViewActivity extends AppCompatActivity {


    private RecyclerView recyclerView;
    private MyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        final EditText sizeInput = findViewById(R.id.id_size_edit);

        adapter = new MyAdapter();
        recyclerView =  findViewById(R.id.id_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        findViewById(R.id.add_items).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int size = Integer.valueOf(String.valueOf(sizeInput.getText()));
                for(int i =1;i<= size; i++){
                    adapter.addItem(new Item(adapter.getItemCount()+1, new Random().nextInt()));
                }


            }
        });

        findViewById(R.id.id_clear_list_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               adapter.reserList();
            }
        });

    }
}
