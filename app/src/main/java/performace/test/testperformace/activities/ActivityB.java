package performace.test.testperformace.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import performace.test.testperformace.Model;
import performace.test.testperformace.R;

public class ActivityB extends AppCompatActivity {

    private TextView counterTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);

        counterTxt = findViewById(R.id.id_counter);
        int count = Model.getInstance().getCounter() + 1;
        counterTxt.setText(count+"");

        findViewById(R.id.toA).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int counter = Model.getInstance().getCounter();
                counter++;
                Model.getInstance().setCounter(counter);
                Intent intent = new Intent(ActivityB.this, ActivityA.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                startActivity(intent);
            }
        });

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int counter = Model.getInstance().getCounter();
                counter--;
                Model.getInstance().setCounter(counter);
                ActivityB.this.finish();
            }
        });
    }
}
