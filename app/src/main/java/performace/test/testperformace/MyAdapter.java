package performace.test.testperformace;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{

    private List<Item> values;

    public MyAdapter(List<Item> values) {
        this.values = values;
    }

    public MyAdapter() {
      if(values== null){
          values = new ArrayList<>();
      }
    }

    public void addItem(Item item){
        if(values== null){
            values = new ArrayList<>();
        }
        values.add(item);
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Item item = values.get(position);
        holder.numOfItem.setText(item.getNumOfLine()+"");
        holder.randomNum.setText(item.getRandValue()+"");
    }

    @Override
    public int getItemCount()  {
        return values.size();
    }

    public void reserList() {
        values = new ArrayList<>();
        this.notifyDataSetChanged();
    }

    public class MyViewHolder  extends RecyclerView.ViewHolder{
        TextView numOfItem;
        TextView randomNum;

        public MyViewHolder(View itemView) {
            super(itemView);
            numOfItem = itemView.findViewById(R.id.id_numOfItem);
            randomNum = itemView.findViewById(R.id.id_randValiue);
        }
    }
}
